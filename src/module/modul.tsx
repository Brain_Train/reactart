export type Post = {
    title: string;
    text: string;
    breed: string;
}

const AnimalsPosts: Post[] = [
    {
        breed: 'Pug',
        title: 'Dogs',
        text: 'The pug is a breed of dog originally from China, with physically distinctive features of a wrinkly, short-muzzled face and curled tail. The breed has a fine, glossy coat that comes in a variety of colors, most often light brown (fawn) or black, and a compact, square body with well-developed muscles.'
    },
    {
        breed: 'British cat',
        title: 'Cats',
        text: 'The cat (Felis catus) is a domestic species of small carnivorous mammal. It is the only domesticated species in the family Felidae and is often referred to as the domestic cat to distinguish it from the wild members of the family.'
    },
    {
        breed: 'Сockatoo',
        title: 'Parrots',
        text: 'A cockatoo is any of the 21 parrot species belonging to the family Cacatuidae, the only family in the superfamily Cacatuoidea.'
    },
    {
        breed: 'Clown fish',
        title: 'Fish',
        text: 'Clownfish or anemonefish are fishes from the subfamily Amphiprioninae in the family Pomacentridae.'
    },
]

export default AnimalsPosts;