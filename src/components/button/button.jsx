import './buttom.scss';

const Button = (props) => {
    return (
        <button
            id={props.id}
            onClick={props.handleClick}
            className={props.className}
            value={props.value}
        >
            {props.btnText}
        </button>
    )
}

export default Button;
