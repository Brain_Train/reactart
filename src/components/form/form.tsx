import React, { ChangeEvent, Dispatch, SetStateAction, useState } from 'react';

import AnimalsPosts from '../../module/modul';
import { Post } from '../../module/modul';
import ShowItems from '../../components/items/items';
import Button from '../button/button';

import './form.scss'

const Form: React.FC = () => {
    let info: Post = {
        title: '',
        breed: '',
        text: ''
    }
    let [post, setPost]: [Post, Dispatch<SetStateAction<Post>>] = useState(info);

    const openInformation = (e: ChangeEvent<HTMLInputElement>) => {
        let info = AnimalsPosts.find(post => post.breed === (e.target!).value)!;
        setPost(info)
    }

    return (
        <div className='container'>
            <div className='information'>
                {AnimalsPosts.map(post => {
                    return (
                    <div>
                        <Button
                            className="btn"
                            handleClick={openInformation}
                            value={post.breed}
                            btnText={post.title}
                        />
                    </div>)
                })}
            </div>
            {<ShowItems post={post} />}
        </div>
    );
}

export default Form;