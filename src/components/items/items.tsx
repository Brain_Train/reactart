import { Post } from '../../module/modul';

import './items.scss'

const ShowItems = (props: { post: Post }) => {

    if (props.post.breed.length) {
        return <div className="containers">
                <div className='title'>
                    <h3>Animal: {props.post.title}</h3>
                </div>
                <div className='title'>
                    <h3>Breed: {props.post.breed}</h3>
                </div>
            <div >
                <div>{props.post.text}</div>
            </div>
        </div>;
    }
    return <div />;
}

export default ShowItems;